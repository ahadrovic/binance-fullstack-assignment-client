# binance-fullstack-assignment-client

This repository contains all the source for the frontend portion of the CMS

This project was developed using React(with functional components and Hooks) and Redux

## A few things to know about this project:

This project showcases the main functional use of the CMS. That is, creating, reading, updating, and deleting articles.

No user authenticaton has been implemented due to the fact that proper security and user management 
would require the use of paid services (eg AWS Cognito) and the configuration of a proper server. 

This project is meant to serve as a demo, run locally, and demonstrate an implementation of fullstack concepts, 
so the user may only use the Article Management feature, which allows the user to carry out CRUD operations on articles.

In order to fully use this CMS demo, binance-fullstack-assignment-api needs to be running separately
	
Rows of articles in the table represent the an article, along with its translations
	
Since this is a demo, articles do not publish to a website. Instead, the CMS can generate previews

# Work flow

1. Navigate to Articles
2. Click the + Article button
3. Fill in the form data
4. Save the article draft
5. You may either choose to edit an exist article or create a new article
7. The user can access translations of an article by selecting edit and choosing a lanugage
8. Selecting a language that hasn't been used on the article will switch the form to create mode
9. Clicking delete will open a modal to confirm that the user wants to delete the article and it's translations
	single translation delete has not been implemented in this demo, but it would be available in a final product
10. Clicking Publish will set the Date Published value and give access to a preview of the article and its translations
11. The article can be previewed using the following url pattern: /preview/:lang/:slug
	slug is the article's URL slug, lang indicates the lanugage abbreviations of the translation to be viewed

# Project commands

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

