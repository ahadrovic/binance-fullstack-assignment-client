import { combineReducers } from "redux";
import articleManagement from "./articleManagement/reducers";

export default combineReducers({
  articleManagement
});
