import ArticleManagementTypes from "./articleManagementTypes";

import API from "../../services";

export const getArticles = (paginationToken = "") => async (
  dispatch,
  getState
) => {
  dispatch(getArticlesBegin());

  try {
    // to do: get user

    // const { configuration } = getState();
    // const { config } = configuration;
    // const currentUser = await authenticators.getCurrentUser();
    // const accessToken = authenticators.getAccessTokenFromCognitoUser(
    //   currentUser
    // );
    /*

    const response = await API.initiate("localhost").private({
      accessToken
    }).articleManagement.getArticles();
    */

    // to do: upgrade request to HTTPS
    const response = await API.initiate("http://localhost") // for demo purposes, only localhost is to be used
      .public()
      .articleManagement.getArticles(paginationToken);

    const { error } = response;
    if (error) {
      const { message } = error;
      dispatch(
        getArticlesFail({
          message: message || "Some error occured."
        })
      );
      return;
    }
    dispatch(getArticlesSuccess(response, paginationToken));
  } catch (error) {
    dispatch(
      getArticlesFail({
        message: error.message || "Some error occured.",
        errorObject: error
      })
    );
  }
};

const getArticlesBegin = () => {
  return {
    type: ArticleManagementTypes.GET_ARTICLES_BEGIN,
    payload: null
  };
};

const getArticlesFail = error => {
  return {
    type: ArticleManagementTypes.GET_ARTICLES_FAIL,
    payload: {
      error,
      result: null
    }
  };
};

const getArticlesSuccess = (response, paginationToken) => {
  const {
    GET_ARTICLES_PAGE_SUCCESS,
    GET_ARTICLES_SUCCESS
  } = ArticleManagementTypes;
  const reducerType = paginationToken
    ? GET_ARTICLES_PAGE_SUCCESS
    : GET_ARTICLES_SUCCESS;
  return {
    type: reducerType,
    payload: {
      error: null,
      result: response
    }
  };
};

export const createArticle = inputArticle => async (dispatch, getState) => {
  dispatch(createArticleBegin());

  try {
    // to do: get user

    // const { configuration } = getState();
    // const { config } = configuration;
    // const currentUser = await authenticators.getCurrentUser();
    // const accessToken = authenticators.getAccessTokenFromCognitoUser(
    //   currentUser
    // );
    /*

    const response = await API.initiate("localhost").private({
      accessToken
    }).articleManagement.getArticles();
    */

    // to do: upgrade request to HTTPS
    const response = await API.initiate("http://localhost") // for demo purposes, only localhost and http is used
      .public()
      .articleManagement.createArticle(inputArticle);

    const { error } = response;
    if (error) {
      const { message } = error;
      dispatch(
        createArticleFail({
          message: message || "Some error occured."
        })
      );
      return;
    }
    dispatch(createArticleSuccess(response));
  } catch (error) {
    dispatch(
      getArticlesFail({
        message: error.message || "Some error occured.",
        errorObject: error
      })
    );
  }
};

const createArticleBegin = () => {
  return {
    type: ArticleManagementTypes.CREATE_ARTICLE_BEGIN,
    payload: null
  };
};

const createArticleFail = error => {
  return {
    type: ArticleManagementTypes.CREATE_ARTICLE_FAIL,
    payload: {
      error,
      result: null
    }
  };
};

const createArticleSuccess = response => {
  return {
    type: ArticleManagementTypes.CREATE_ARTICLE_SUCCESS,
    payload: {
      error: null,
      result: response
    }
  };
};

export const updateArticle = (inputArticle, mode) => async (
  dispatch,
  getState
) => {
  dispatch(updateArticleBegin());

  try {
    // to do: get user

    // const { configuration } = getState();
    // const { config } = configuration;
    // const currentUser = await authenticators.getCurrentUser();
    // const accessToken = authenticators.getAccessTokenFromCognitoUser(
    //   currentUser
    // );
    /*

    const response = await API.initiate("localhost").private({
      accessToken
    }).articleManagement.getArticles();
    */

    const response = await API.initiate("http://localhost") // for demo purposes, only localhost and http is used
      .public()
      .articleManagement.updateArticle(inputArticle);

    const { error } = response;
    if (error) {
      const { message } = error;
      dispatch(
        updateArticleFail({
          message: message || "Some error occured."
        })
      );
      return;
    }
    dispatch(updateArticleSuccess(response, mode));
    return response;
  } catch (error) {
    dispatch(
      updateArticleFail({
        message: error.message || "Some error occured.",
        errorObject: error
      })
    );
  }
};

const updateArticleBegin = () => {
  return {
    type: ArticleManagementTypes.UPDATE_ARTICLE_BEGIN,
    payload: null
  };
};

const updateArticleFail = error => {
  return {
    type: ArticleManagementTypes.UPDATE_ARTICLE_FAIL,
    payload: {
      error,
      result: null
    }
  };
};

const updateArticleSuccess = (response, mode) => {
  return {
    type: ArticleManagementTypes.CREATE_ARTICLE_SUCCESS,
    payload: {
      error: null,
      result: response,
      mode: mode || "UPDATE"
    }
  };
};

export const deleteArticle = inputArticleId => async (dispatch, getState) => {
  dispatch(deleteArticleBegin());

  try {
    // to do: get user

    // const { configuration } = getState();
    // const { config } = configuration;
    // const currentUser = await authenticators.getCurrentUser();
    // const accessToken = authenticators.getAccessTokenFromCognitoUser(
    //   currentUser
    // );
    /*

    const response = await API.initiate("localhost").private({
      accessToken
    }).articleManagement.getArticles();
    */

    const response = await API.initiate("http://localhost") // for demo purposes, only localhost and http is used
      .public()
      .articleManagement.deleteArticle(inputArticleId);

    const { error } = response || {};
    if (error) {
      const { message } = error;
      dispatch(
        deleteArticleFail({
          message: message || "Some error occured."
        })
      );
      return;
    }
    dispatch(deleteArticleSuccess(response, inputArticleId));
    return response;
  } catch (error) {
    dispatch(
      deleteArticleFail({
        message: error.message || "Some error occured.",
        errorObject: error
      })
    );
  }
};

const deleteArticleBegin = () => {
  return {
    type: ArticleManagementTypes.DELETE_ARTICLE_BEGIN,
    payload: null
  };
};

const deleteArticleFail = error => {
  return {
    type: ArticleManagementTypes.DELETE_ARTICLE_FAIL,
    payload: {
      error,
      result: null
    }
  };
};

const deleteArticleSuccess = (response, inputArticleId) => {
  return {
    type: ArticleManagementTypes.UPDATE_ARTICLE_SUCCESS,
    payload: {
      error: null,
      result: { response, deleteArticleId: inputArticleId }
    }
  };
};

export const finishOperation = () => async (dispatch, getState) => {
  dispatch(finishOperationSuccess());
  return;
};

const finishOperationSuccess = () => {
  return {
    type: ArticleManagementTypes.RESET_ARTICLE_SUCCESS,
    payload: {
      error: null,
      result: null
    }
  };
};
