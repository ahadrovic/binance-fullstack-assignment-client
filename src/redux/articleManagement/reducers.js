import ArticleManagementTypes from "./articleManagementTypes";
import _ from "lodash";

const initialState = {
  selectedArticles: [],
  processingArticles: false,
  articlesMessage: "",
  articlePaginationKey: "",
  articleRecords: [],
  result: null,
  error: null
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case ArticleManagementTypes.GET_ARTICLES_BEGIN:
      return {
        ...state,
        articlesMessage: "",
        processingArticles: true
      };

    case ArticleManagementTypes.GET_ARTICLES_FAIL:
      return {
        ...state,
        processingArticles: false,
        error: payload.error
      };

    case ArticleManagementTypes.GET_ARTICLES_SUCCESS:
      const { result: getArticlesResult } = payload;

      const groupedArticlesObject = _.groupBy([...getArticlesResult], "slug");
      const groupKeys = Object.keys(groupedArticlesObject);

      const firstArticlesFromGroups = groupKeys.map(key => {
        const currentGroup = groupedArticlesObject[key];
        const availableLanguages = currentGroup.map(
          article => article.language
        );

        const firstArticle =
          currentGroup.find(article => article.language === "en") ||
          currentGroup.find(
            article => article.language === availableLanguages[0]
          );
        firstArticle.languages = availableLanguages.join(",");

        return firstArticle;
      });

      const articleRecords = firstArticlesFromGroups.map((article, index) => {
        const articleKeys = Object.keys(article);
        const objectToReturn = {
          key: index
        };

        articleKeys.forEach(key => {
          if (key.toLowerCase().includes("date")) {
            if (key === "datePublished") {
              objectToReturn[key] =
                article[key] !== 0
                  ? new Date(parseInt(article[key])).toString()
                  : "Not Published";
            } else {
              objectToReturn[key] = new Date(parseInt(article[key])).toString();
            }
          } else {
            objectToReturn[key] = article[key];
          }
        });
        objectToReturn.key = objectToReturn.id;
        return objectToReturn;
      });

      return {
        ...state,
        articleRecords: [...articleRecords],
        processingArticles: false,
        // articlePaginationKey: paginationKeyArticle || "",
        result: getArticlesResult
      };

    case ArticleManagementTypes.CREATE_ARTICLE_BEGIN:
      return {
        ...state,
        error: null,
        articlesMessage: "",
        processingArticles: true
      };

    case ArticleManagementTypes.CREATE_ARTICLE_FAIL:
      return {
        ...state,
        processingArticles: false,
        error: payload.error
      };

    case ArticleManagementTypes.CREATE_ARTICLE_SUCCESS:
      const { result: createArticleResult, mode } = payload;
      const { articleRecords: prevArticleRecords } = state;
      let articleRecordsAfterCreate = [];

      const matchingSlugArticleRecord = prevArticleRecords.find(
        articleRecord => articleRecord.slug === createArticleResult.slug
      );

      const excludedArticles = prevArticleRecords.filter(
        articleRecord => articleRecord.slug !== createArticleResult.slug
      );

      if (matchingSlugArticleRecord) {
        const newLanguages =
          mode === "UPDATE" || mode === "PUBLISH"
            ? matchingSlugArticleRecord.languages
            : `${matchingSlugArticleRecord.languages},${createArticleResult.language}`;
        matchingSlugArticleRecord.languages = newLanguages;

        matchingSlugArticleRecord.dateModified =
          mode === "UPDATE" || mode === "PUBLISH"
            ? new Date(parseInt(createArticleResult.dateModified)).toString()
            : new Date(parseInt(createArticleResult.dateCreated)).toString();
        articleRecordsAfterCreate = [
          ...excludedArticles,
          matchingSlugArticleRecord
        ];
        matchingSlugArticleRecord.datePublished =
          createArticleResult.datePublished !== 0
            ? new Date(parseInt(createArticleResult.datePublished)).toString()
            : "Not Published";
      } else {
        const newArticleRecord = {
          ...createArticleResult
        };
        newArticleRecord.key = newArticleRecord.id;
        newArticleRecord.languages = newArticleRecord.language;
        newArticleRecord.datePublished =
          newArticleRecord.datePublished === 0
            ? "Not Published"
            : newArticleRecord.datePublished;
        articleRecordsAfterCreate = [...prevArticleRecords, newArticleRecord];
      }

      let createMessage = `${createArticleResult.language} Article created successfully.`;
      switch (mode) {
        case "UPDATE":
          createMessage = createMessage.replace("created", "updated");
          break;
        case "PUBLISH":
          createMessage = createMessage.replace("created", "published");
          break;
        default:
          break;
      }

      return {
        ...state,
        result: [...(state.result || []), createArticleResult],
        articleRecords: articleRecordsAfterCreate,
        articlesMessage: createMessage,
        processingArticles: false
      };

    case ArticleManagementTypes.UPDATE_ARTICLE_BEGIN:
      return {
        ...state,
        error: null,
        articlesMessage: "",
        processingArticles: true
      };

    case ArticleManagementTypes.UPDATE_ARTICLE_FAIL:
      return {
        ...state,
        processingArticles: false,
        articleMessage: "",
        error: payload.error
      };

    case ArticleManagementTypes.DELETE_ARTICLE_BEGIN:
      return {
        ...state,
        error: null,
        articleMessage: "",
        processingArticles: true
      };

    case ArticleManagementTypes.DELETE_ARTICLE_FAIL:
      return {
        ...state,
        error: payload.error,
        articleMessage: "",
        processingArticles: false
      };

    case ArticleManagementTypes.DELETE_ARTICLE_SUCCESS:
      const { result: deleteArticleResult } = payload;
      const { deleteArticleId } = deleteArticleResult;

      let deleteMessage = "";
      const deletedArticle = state.result.find(
        article => article.id === deleteArticleId
      );

      const filteredResults = state.result.filter(
        article => article.id !== deleteArticleId
      );

      const newArticleRecords = [...state.articleRecords];
      let foundArticle = null;
      const foundArticleRecordIndex = newArticleRecords.findIndex(
        articleRecord => articleRecord.slug === deletedArticle.slug
      );

      if (foundArticleRecordIndex !== -1) {
        foundArticle = newArticleRecords[foundArticleRecordIndex];
        const splitLanguages = foundArticle.languages.split(",");
        const matchingLangIndex = splitLanguages.findIndex(
          lang => lang === deletedArticle.language
        );
        if (matchingLangIndex !== -1) {
          deleteMessage = `Article translation for ${splitLanguages[matchingLangIndex]} has been deleted`;
          splitLanguages.splice(matchingLangIndex, 1);
          foundArticle.languages = splitLanguages.join();
        }
      }

      if (foundArticle && !foundArticle.languages.trim()) {
        newArticleRecords.splice(foundArticleRecordIndex, 1);
        deleteMessage =
          "The Article and all of it's tranlsations have been deleted";
      }

      return {
        ...state,
        result: filteredResults,
        articleRecords: newArticleRecords,
        articlesMessage: deleteMessage,
        processingArticles: false
      };

    case ArticleManagementTypes.RESET_ARTICLE_SUCCESS:
      return {
        ...state,
        articlesMessage: "",
        processingArticles: false
      };

    default:
      return {
        ...initialState
      };
  }
};
