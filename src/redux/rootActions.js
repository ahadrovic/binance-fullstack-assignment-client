import * as articleManagementActions from "./articleManagement/actions";

export default {
  articleManagement: {
    ...articleManagementActions
  }
};
