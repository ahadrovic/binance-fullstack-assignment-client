export const languages = [
  {
    id: "1",
    name: "English",
    abbreviation: "en"
  },
  {
    id: "2",
    name: "Français",
    abbreviation: "fr"
  },
  {
    id: "3",
    name: "Deutsch",
    abbreviation: "de"
  },
  {
    id: "4",
    name: "Polski",
    abbreviation: "pl"
  },
  {
    id: "5",
    name: "简体中文",
    abbreviation: "zh"
  },
  {
    id: "6",
    name: "繁體中文",
    abbreviation: "zt"
  },
  {
    id: "7",
    name: "Tiếng Việt",
    abbreviation: "vi"
  },
  {
    id: "8",
    name: "العربي",
    abbreviation: "ar"
  },
  {
    id: "9",
    name: "한국어<",
    abbreviation: "ko"
  },
  {
    id: "10",
    name: "Русский",
    abbreviation: "ru"
  },
  {
    id: "11",
    name: "Español",
    abbreviation: "es"
  },
  {
    id: "12",
    name: "Türkçe",
    abbreviation: "tr"
  },
  {
    id: "13",
    name: "Nederlands",
    abbreviation: "nl"
  },
  {
    id: "14",
    name: "Português",
    abbreviation: "pt"
  },
  {
    id: "15",
    name: "Italiano",
    abbreviation: "it"
  },
  {
    id: "16",
    name: "Bahasa Indonesia",
    abbreviation: "id"
  },
  {
    id: "17",
    name: "日本語",
    abbreviation: "ja"
  }
];

// for demo purposes, we will use static categories with a few languages
// this is how the data would look coming from a service
export const categories = [
  {
    id: "1",
    name: "Blockchain",
    text: "Blockchain",
    languages: languages
      .map(language => language.abbreviation)
      .filter(lang => lang.abbreviation !== "zh")
  },
  {
    id: "2",
    name: "Economics",
    text: "Economics",
    languages: ["en"]
  },
  {
    id: "3",
    name: "Security",
    text: "Security",
    languages: ["en"]
  },
  {
    id: "4",
    name: "Tutorials",
    text: "Tutorials",
    languages: ["en"]
  },
  {
    id: "5",
    name: "Economics",
    text: "Economie",
    languages: ["fr"]
  },
  {
    id: "6",
    name: "Security",
    text: "Sécurité",
    languages: ["fr"]
  },
  {
    id: "7",
    name: "Blockchain",
    text: "区块链",
    languages: ["zh"]
  },
  {
    id: "8",
    name: "Economics",
    text: "经济",
    languages: ["zh"]
  },
  {
    id: "9",
    name: "Security",
    text: "安全",
    languages: ["zh"]
  },
  {
    id: "10",
    name: "Tutorials",
    text: "教程",
    languages: ["zh"]
  }
];

export const initialContent = {
  entityMap: {},
  blocks: [
    {
      key: "637gr",
      text: "Initialized from content state.",
      type: "unstyled",
      depth: 0,
      inlineStyleRanges: [],
      entityRanges: [],
      data: {}
    }
  ]
};
