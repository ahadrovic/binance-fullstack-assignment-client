import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Layout, Menu, Icon } from "antd";
import "antd/dist/antd.css";
import "../App.css";
import "../index.css";
import logo from "../binance-horizontal.svg";
import ArticleManagement from "./ArticleManagement";
import ArticlePresentation from "./../screens/ArticleManagement/ArticlePresentation";
import CategoryManagement from "./CategoryManagement";
import CollectionManagement from "./CollectionManagement";
import Dashboard from "./Dashboard";

const { Header, Content, Footer, Sider } = Layout;

const App = () => {
  const [collapsed, setCollapse] = useState(false);

  return (
    <Router>
      <Layout style={{ minHeight: "100vh" }}>
        <Sider
          collapsible
          collapsed={collapsed}
          onCollapse={newCollapsed => {
            setCollapse(newCollapsed);
          }}
        >
          <div>
            <img src={logo} alt="Binance logo" />
          </div>
          <Menu theme="dark" mode="inline">
            <Menu.Item key="1">
              <Link to="/home">
                <Icon className="nav-link-icon" type="home" />
                <span className="nav-link">Home</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/articles">
                <Icon className="nav-link-icon" type="file" />
                <span className="nav-link">Articles</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Link to="/categories">
                <Icon className="nav-link-icon" color="" type="folder" />
                <span className="nav-link">Categories</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="4">
              <Link to="/collections">
                <Icon className="nav-link-icon" type="appstore" />
                <span className="nav-link">Collections</span>
              </Link>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ padding: 0 }} />
          <Content style={{ margin: "0 16px" }}>
            <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
              <Switch>
                <Route path="/home">
                  <Dashboard />
                </Route>
                <Route
                  path="/articles/preview/:lang/:slug"
                  children={<ArticlePresentation />}
                />
                <Route path="/articles/">
                  <ArticleManagement />
                </Route>
                <Route path="/categories">
                  <CategoryManagement />
                </Route>
                <Route path="/collections">
                  <CollectionManagement />
                </Route>
                <Route component={Dashboard} />
              </Switch>
            </div>
          </Content>
          <Footer style={{ textAlign: "center" }}>
            Binance Article CMS Demo - Adem Hadrovic ©2019
          </Footer>
        </Layout>
      </Layout>
    </Router>
  );
};

export default App;
