import React, { useEffect, useState, useRef } from "react";
import { Row, Col, Select, message as AntMessage } from "antd";
import { useParams } from "react-router-dom";
import { connect } from "react-redux";
import { Editor } from "react-draft-wysiwyg";

import actions from "../../redux/rootActions";

import { languages } from "../../constants";

const languageOptions = languages.map(language => ({
  key: language.id,
  value: language.abbreviation,
  text: language.name
}));

const ArticlePresentation = ({ getArticles, result: articleResults }) => {
  const { lang, slug } = useParams();
  console.log("lang:", lang);
  const [currentArticle, setCurrentArticle] = useState(null);
  const [selectedLanguageId, setSelectedLanguageId] = useState();
  const articleLanguageFieldRef = useRef(null);
  useEffect(() => {
    // to do, getArticles based on url0-slug only
    getArticles();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (articleResults && articleResults.length > 0 && !currentArticle) {
      const foundArticle = articleResults.find(
        article => article.language === lang && article.slug === slug
      );
      setCurrentArticle(foundArticle);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [articleResults]);
  return (
    <div>
      <Row wi>
        <Col span={6} style={{ float: "right" }}>
          <Select
            ref={articleLanguageFieldRef}
            key="article-language-input"
            value={selectedLanguageId}
            placeholder={`${
              languageOptions.find(langOpt => langOpt.value === lang).text
            }`}
            onChange={value => {
              setSelectedLanguageId(value);
              const foundLangOpt = languageOptions.find(
                langOpt => langOpt.key === value
              );

              if (foundLangOpt) {
                const foundArticle = articleResults.find(
                  article =>
                    article.language === foundLangOpt.value &&
                    article.slug === slug
                );
                if (foundArticle) {
                  setCurrentArticle(foundArticle);
                } else {
                  AntMessage.warn("Article not found");
                  setCurrentArticle(null);
                }
              } else {
                AntMessage.warn("Language not found");
              }
            }}
            style={{ width: "100%" }}
          >
            {languageOptions.map(language => (
              <Select.Option key={language.key}>{language.text}</Select.Option>
            ))}
          </Select>
        </Col>
      </Row>
      <br />
      {!currentArticle && (
        <div>
          <h2>Article Not Found</h2>
          <span>You may have not published the article yet</span>
        </div>
      )}
      {currentArticle && (
        <div>
          {currentArticle.imageUrl && (
            <img src={currentArticle.imageUrl} alt={"Article Banner"} />
          )}
          <h1>{currentArticle.title}</h1>
          <h2>{currentArticle.description}</h2>
          <h4>Written by: {currentArticle.author}</h4>
          <h4>
            Published: {new Date(currentArticle.datePublished).toString()}
          </h4>
          {currentArticle.videoUrl && (
            <div>
              <hr />
              <iframe
                title="Related Video"
                width="640"
                height="400"
                src={currentArticle.videoUrl}
              ></iframe>
            </div>
          )}
          <hr />
          <div>
            <Editor
              toolbarHidden
              contentState={JSON.parse(currentArticle.content)}
              readOnly
            />
          </div>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = state => {
  const { articleManagement } = state;

  return {
    ...articleManagement
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getArticles: (paginationToken = "") => {
      dispatch(actions.articleManagement.getArticles(paginationToken));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ArticlePresentation);
