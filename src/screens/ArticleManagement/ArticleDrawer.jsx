import { Button, Col, Drawer, Form, Input, Row, Select, Tooltip } from "antd";
import React, { useRef, useState } from "react";
import { Editor } from "react-draft-wysiwyg";

import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

import { languages, categories, initialContent } from "../../constants";

const ArticleDrawer = ({
  mode,
  setMode,
  articleToEdit,
  setArticleToEdit,
  articleGroupToEdit,
  setArticleGroupToEdit,
  showAddArticleDrawer,
  toggleAddArticleDrawer,
  toggleConfirmChangesModal
}) => {
  const articleLanguageFieldRef = useRef(null);
  const articleTitleFieldRef = useRef(null);
  const articleCategoryFieldRef = useRef(null);
  const articleSlugFieldRef = useRef(null);
  const articleDescriptionFieldRef = useRef(null);
  const articleImageUrlFieldRef = useRef(null);
  const articleVideoUrlFieldRef = useRef(null);
  const contentEditorRef = useRef(null);

  const languageOptions = languages.map(language => ({
    key: language.id,
    value: language.abbreviation,
    text: language.name
  }));

  const [selectedLanguageId, setSelectedLanguageId] = useState("");
  const [selectedCategoryId, setSelectedCategoryId] = useState("");

  const categoryOptions = categories
    .map(category => ({
      key: category.id,
      value: category.text,
      text: category.text,
      languages: category.languages.join(",")
    }))
    .filter(categoryOption => {
      if (selectedLanguageId || (mode === "UPDATE" && articleToEdit.language)) {
        const foundLanguageOption = languageOptions.find(langOpt => {
          if (articleToEdit.language) {
            return langOpt.value === articleToEdit.language;
          }
          return langOpt.key === selectedLanguageId;
        });
        return (
          foundLanguageOption &&
          categoryOption.languages.includes(foundLanguageOption.value)
        );
      } else {
        return categoryOption.languages.includes("en");
      }
    });

  const setArticleToEditProp = (key, value) => {
    if (articleGroupToEdit.length > 0 && key === "language") {
      const foundLanguage = languages.find(lang => lang.id === value);
      const foundArticle = articleGroupToEdit.find(
        article => article.language === foundLanguage.abbreviation
      );
      if (foundArticle) {
        setMode("UPDATE");
        setArticleToEdit({ ...foundArticle });
      } else {
        setSelectedLanguageId(value);
        const newArticleTranslation = {
          ...articleToEdit,
          id: `${Math.round(Math.random() * 10000000)}`,
          category: "", // manually select
          language: foundLanguage.abbreviation
        };
        setMode("CREATE");
        setArticleToEdit({ ...newArticleTranslation });
      }
    } else {
      let newArticleToEdit = { ...articleToEdit };
      if (key === "language" && mode !== "UPDATE") {
        const foundLanguage = languages.find(lang => lang.id === value);
        newArticleToEdit.language = foundLanguage
          ? foundLanguage.abbreviation
          : "en";
        const foundCategory = categories.find(
          cat =>
            cat.languages.filter(lang => lang === foundLanguage.abbreviation)
              .length > 1
        );

        if (!foundCategory) {
          setSelectedCategoryId("");
          newArticleToEdit.category = "";
        }
      } else if (key === "category") {
        const foundCategory = categories.find(cat => cat.id === value);
        newArticleToEdit.category = foundCategory ? foundCategory.text : "";
      } else {
        newArticleToEdit[key] = value;
      }
      setArticleToEdit(newArticleToEdit);
    }
  };

  // This is an attempt at changing the initial value of the content editor
  // Sadly, it does not work at the moment

  // const [defaultContentState, setDefaultContentState] = useState("");
  // useEffect(() => {
  //   if (showAddArticleDrawer && mode === "UPDATE" && articleToEdit.content) {
  //     setDefaultContentState(JSON.parse(articleToEdit.content));
  //   } else {
  //     setDefaultContentState("");
  //   }
  // }, [showAddArticleDrawer, mode, articleToEdit.content]);

  const editorProps = {
    ref: contentEditorRef,
    // defaultContentState: defaultContentState, // the default state does not change dynamically.
    // a possible solution would be to use a redux prop and dispatch
    onContentStateChange: contentState => {
      setArticleToEditProp("content", JSON.stringify(contentState));
    }
  };

  return (
    <Drawer
      title={mode === "UPDATE" ? "Edit Article" : "Add Article"}
      width={960}
      onClose={() => {
        toggleAddArticleDrawer(false);
        setMode("");
        setArticleToEdit({ content: JSON.stringify(initialContent) });
        setArticleGroupToEdit([]);
      }}
      visible={showAddArticleDrawer}
    >
      <Form layout="vertical" hideRequiredMark={true}>
        <Row>
          <Col span={6}>
            <Form.Item label="Select Article Language" required>
              <Select
                ref={articleLanguageFieldRef}
                key="article-language-input"
                value={
                  mode === "UPDATE" && articleToEdit.language
                    ? languages.find(
                        lang => lang.abbreviation === articleToEdit.language
                      ).id || selectedLanguageId
                    : selectedLanguageId
                }
                placeholder="Select a Language"
                onChange={value => {
                  setSelectedLanguageId(value);
                  setArticleToEditProp("language", value);
                }}
                style={{ width: "100%" }}
              >
                {languageOptions.map(language => (
                  <Select.Option key={language.key}>
                    {language.text}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="Enter Article Name" required>
              <Input
                ref={articleTitleFieldRef}
                key="article-title-input"
                required={true}
                value={articleToEdit.title}
                onChange={event => {
                  const { value } = event.target;
                  setArticleToEditProp("title", value);
                }}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Select a Category" required>
              <Select
                ref={articleCategoryFieldRef}
                key="article-categoy-input"
                value={
                  mode === "UPDATE" && articleToEdit.category
                    ? categories.find(
                        cat => cat.text === articleToEdit.category
                      ).id
                    : selectedCategoryId
                }
                placeholder="Select a Category"
                onChange={value => {
                  setSelectedCategoryId(value);
                  setArticleToEditProp("category", value);
                }}
              >
                {categoryOptions.map(category => (
                  <Select.Option key={category.key}>
                    {category.text}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="Enter Article URL Slug">
              <Tooltip
                trigger={"focus"}
                title={"There is no need to add /"}
                placement="topLeft"
              >
                <Input
                  ref={articleSlugFieldRef}
                  key="article-slug-input"
                  required={true}
                  value={articleToEdit.slug}
                  onChange={event => {
                    const { value } = event.target;
                    setArticleToEditProp("slug", value.toLowerCase());
                  }}
                />
              </Tooltip>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Enter Article Description" required>
              <Input.TextArea
                type="text"
                ref={articleDescriptionFieldRef}
                key="article-description-input"
                required={true}
                value={articleToEdit.description}
                onChange={event => {
                  const { value } = event.target;
                  setArticleToEditProp("description", value);
                }}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="Header Image URL">
              <Input
                ref={articleImageUrlFieldRef}
                key="article-imageUrl-input"
                required={true}
                value={mode === "UPDATE" ? articleToEdit.imageUrl : undefined}
                onChange={event => {
                  const { value } = event.target;
                  setArticleToEditProp("imageUrl", value);
                }}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Related Video URL">
              <Input
                ref={articleVideoUrlFieldRef}
                key="article-videoUrl-input"
                required={true}
                value={mode === "UPDATE" ? articleToEdit.videoUrl : undefined}
                onChange={event => {
                  const { value } = event.target;
                  setArticleToEditProp("videoUrl", value);
                }}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={24}>
            <Form.Item label="Article Content" required>
              {/* to do: fix editor - does not populate input properly whne the user edits*/}
              <Tooltip
                trigger={"focus"}
                title={
                  "Inputting content and saving in edit mode will overwtite the current content completely"
                }
                placement="topLeft"
              >
                <Editor {...editorProps} />
              </Tooltip>
            </Form.Item>
          </Col>
        </Row>
      </Form>
      <br />
      <div
        style={{
          position: "absolute",
          left: 0,
          bottom: 0,
          width: "100%",
          borderTop: "1px solid #e9e9e9",
          padding: "10px 16px",
          background: "#fff",
          textAlign: "right"
        }}
      >
        <Button
          key="cancel-article-changes-button"
          onClick={() => {
            toggleAddArticleDrawer(false);
            setMode("");
            setArticleToEdit({ content: JSON.stringify(initialContent) });
            setArticleGroupToEdit([]);
          }}
          style={{ marginRight: 8 }}
        >
          Cancel
        </Button>
        <Button
          key="submit-article-changes-button"
          type="primary"
          onClick={() => {
            toggleConfirmChangesModal(true);
          }}
        >
          Save Draft
        </Button>
      </div>
    </Drawer>
  );
};

export default ArticleDrawer;
