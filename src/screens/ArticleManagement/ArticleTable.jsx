import React, { useEffect, useState } from "react";
import { Table, Row, Col, Button, message as AntMessage } from "antd";

import { languages } from "../../constants";

const sortRecords = (a, b, key) => {
  if (a[key] < b[key]) {
    return -1;
  }
  if (a[key] > b[key]) {
    return 1;
  }
  return 0;
};

const ArticleTable = ({
  articles,
  setMode,
  toggleAddArticleDrawer,
  toggleConfirmChangesModal,
  setArticleGroupToEdit,
  processingArticles
}) => {
  const columns = [
    {
      title: "Article Title",
      dataIndex: "title",
      sorter: (a, b) => sortRecords(a, b, "title"),
      sortDirections: ["descend", "ascend"]
    },
    {
      title: "Description",
      dataIndex: "description",
      sortDirections: ["descend", "ascend"]
    },
    {
      title: "URL Slug",
      dataIndex: "slug",
      sorter: (a, b) => sortRecords(a, b, "slug"),
      sortDirections: ["descend", "ascend"]
    },
    {
      title: "Category",
      dataIndex: "category",
      // to do: add filter here? -> filter will pull categories and then set them
      sorter: (a, b) => sortRecords(a, b, "category"),
      sortDirections: ["descend", "ascend"]
    },
    {
      title: "Author",
      dataIndex: "author",
      sorter: (a, b) => sortRecords(a, b, "author"),
      sortDirections: ["descend", "ascend"]
    },
    {
      title: "Languages",
      dataIndex: "languages",
      filters: languages.map(language => ({
        text: language.abbreviation,
        value: language.abbreviation
      })),
      onFilter: (value, record) => {
        const { languages } = record;
        const splitLanguages = languages.split(",");
        return splitLanguages.filter(lang => lang === value).length > 0;
      }
    },
    {
      title: "Date Modified",
      dataIndex: "dateModified",
      sorter: (a, b) => sortRecords(a, b, "dateModified"),
      sortDirections: ["descend", "ascend"]
    },
    {
      title: "Date Published",
      dataIndex: "datePublished",
      sorter: (a, b) => sortRecords(a, b, "datePublished")
    },
    {
      title: "Actions",
      dataIndex: "actions",

      render: (text, record) => (
        <Row style={{ textAlign: "center" }} gutter={[16, 8]}>
          <Col>
            <Button
              key="preview-article-button"
              disabled={processingArticles}
              type="link"
              href={
                record.datePublished !== "Not Published"
                  ? `${window.location.href}/preview/${record.languages.split(
                      ","
                    )[0] || "en"}/${record.slug}`
                  : ""
              }
              onClick={() => {
                if (record.datePublished === "Not Published") {
                  AntMessage.warn("This article has not been published yet.");
                }
              }}
            >
              Preview
            </Button>
          </Col>
          <Col>
            <Button
              key="publish-article-button"
              type="primary"
              disabled={processingArticles}
              onClick={() => {
                setArticleGroupToEdit(record);
                setMode("PUBLISH");
                toggleConfirmChangesModal(true);
              }}
            >
              Publish
            </Button>
          </Col>
          <Col>
            <Button
              key="edit-article-button"
              type="secondary"
              disabled={processingArticles}
              onClick={() => {
                setArticleGroupToEdit(record);
                setMode("UPDATE");
                toggleAddArticleDrawer(true);
              }}
            >
              Edit
            </Button>
          </Col>
          <Col>
            <Button
              key="delete-article-button"
              type="danger"
              disabled={processingArticles}
              onClick={() => {
                setArticleGroupToEdit(record);
                setMode("DELETE");
                toggleConfirmChangesModal(true);
              }}
            >
              Delete
            </Button>
          </Col>
        </Row>
      )
    }
  ];

  const [filteredData, setFilteredData] = useState([]);

  useEffect(() => {
    if (articles && articles.length > 0) {
      setFilteredData(articles);
    }
  }, [articles]);

  return <Table columns={columns} dataSource={filteredData} />;
};

export default ArticleTable;
