import React, { useEffect, useState } from "react";
import {
  Layout,
  Row,
  Col,
  Button,
  Icon,
  Modal,
  Typography,
  Spin,
  message as AntMessage,
  Input,
  Tooltip
} from "antd";

import { connect } from "react-redux";
import actions from "../redux/rootActions";

import ArticleTable from "./ArticleManagement/ArticleTable";
import ArticleDrawer from "./ArticleManagement/ArticleDrawer";

import { initialContent } from "../constants";

const returnConfirmationMessage = mode => {
  switch (mode) {
    case "CREATE":
      return "Click Yes to create the article";
    case "UPDATE":
      return "Are you sure you want to update this article?";
    case "DELETE":
      return "Are you sure you want to delete this article and its translations?";
    case "PUBLISH":
      return "Are you sure you want to publish the article along with its translations?";
    default:
      return "Do you apply these changes?";
  }
};

const ArticleManagement = ({
  getArticles,
  result: articleResults,
  error: articleError,
  articlesMessage,
  articleRecords,
  processingArticles,
  createArticle,
  updateArticle,
  deleteArticle,
  finishOperation
}) => {
  useEffect(() => {
    getArticles();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const [showAddArticleDrawer, toggleAddArticleDrawer] = useState(false);
  const [showConfirmChangesModal, toggleConfirmChangesModal] = useState(false);

  /* 
    example article: 
      "title": "Article Title Two",
      "description": "Article test description",
      "slug": "article-title-two",
      "category": "Cryptoeconomics",
      "language": "en",
      "author": "Adem Hadrovic",
      "content": "",
      "imageUrl": "",
      "videoUrl": "",
      "dateCreated": 1576390080,
      "dateModified": 1576390080,
      "datePublished": 0 
*/

  const [articleToEdit, setArticleToEdit] = useState({
    id: `${Math.round(Math.random() * 10000000)}`,
    title: "",
    description: "",
    slug: "",
    category: "",
    language: "",
    author: "Binance Employee",
    content: JSON.stringify(initialContent),
    imageUrl: "",
    videoUrl: "",
    dateCreated: Date.now().toString(),
    dateModified: Date.now().toString(),
    datePublished: "0"
  });

  const [articleGroupToEdit, setArticleGroupToEdit] = useState([]);

  const [mode, setMode] = useState("");
  const [filteredData, setFilteredData] = useState([]);

  useEffect(() => {
    if (articleError) {
      AntMessage.error(articleError.message);
    }
  }, [articleError]);

  useEffect(() => {
    if (articlesMessage && mode === "") {
      AntMessage.success(articlesMessage);
      setFilteredData(articleRecords);
      finishOperation();
      getArticles();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [articlesMessage, mode, articleRecords]);

  useEffect(() => {
    if (articleRecords && articleRecords.length > 0) {
      setFilteredData(articleRecords);
    }
  }, [articleRecords]);

  const setArticleGroupToEditFromTable = articleRecord => {
    const articleGroup = articleResults.filter(
      article => article.slug === articleRecord.slug
    );
    if (articleGroup) {
      setArticleToEdit({ ...articleGroup[0] });
      setArticleGroupToEdit([...articleGroup]);
    }
  };

  const deleteArticles = async () => {
    if (mode === "DELETE" && articleToEdit.slug) {
      const articleGroup = articleResults.filter(
        article => article.slug === articleToEdit.slug
      );

      // for the demo, all article translations will be deleted when the user clicks delete
      if (articleGroup) {
        await Promise.all(
          articleGroup.map(async article => await deleteArticle(article.id))
        );
        setArticleGroupToEdit([]);
      }
    } else {
      AntMessage.warn("No article has been selected for deletion");
    }
  };

  const publishArticles = async () => {
    if (mode === "PUBLISH" && articleToEdit.slug) {
      if (articleGroupToEdit) {
        await Promise.all(
          articleGroupToEdit.map(async article => {
            const currentEpochTimeString = Date.now().toString();
            const publishedArticle = {
              ...article,
              datePublished: currentEpochTimeString
            };

            return await updateArticle(publishedArticle, "PUBLISH");
          })
        );
      }
    } else {
      AntMessage.warn("No article has been selected for deletion");
    }
  };

  const handleSearch = value => {
    if (value) {
      const filteredArticles = articleRecords.filter(item => {
        return item.title.toLowerCase().includes(value.toLowerCase());
      });
      setFilteredData(filteredArticles);
    } else {
      setFilteredData(articleRecords);
    }
  };

  return (
    <Layout>
      <Layout.Content>
        <Row style={{ backgroundColor: "#fff" }}>
          <Col span={12} style={{ padding: "0.25em" }}>
            <div style={{ float: "left" }}>
              <Tooltip
                trigger={"focus"}
                title={"Search by Article Title"}
                placement="topLeft"
              >
                <Input.Search
                  size="small"
                  placeholder="Search Articles"
                  onSearch={value => handleSearch(value)}
                  style={{ maxWidth: 300 }}
                />
              </Tooltip>
            </div>
          </Col>
          <Col span={12} style={{ padding: 10 }}>
            <Button
              style={{ float: "right" }}
              disabled={processingArticles}
              className="data-management-add-button"
              key="new-article-button"
              type="primary"
              onClick={() => {
                setMode("CREATE");
                toggleAddArticleDrawer(true);
              }}
            >
              <Icon type="plus" /> Atricle
            </Button>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Spin spinning={processingArticles}>
              <ArticleTable
                articles={filteredData}
                setMode={setMode}
                articleToEdit={articleToEdit}
                processingArticles={processingArticles}
                articleGroupToEdit={articleGroupToEdit}
                setArticleGroupToEdit={setArticleGroupToEditFromTable}
                showAddArticleDrawer={showAddArticleDrawer}
                toggleAddArticleDrawer={toggleAddArticleDrawer}
                toggleConfirmChangesModal={toggleConfirmChangesModal}
              />
            </Spin>
          </Col>
        </Row>
        <ArticleDrawer
          mode={mode}
          setMode={setMode}
          showAddArticleDrawer={showAddArticleDrawer}
          toggleAddArticleDrawer={toggleAddArticleDrawer}
          toggleConfirmChangesModal={toggleConfirmChangesModal}
          articleToEdit={articleToEdit}
          articleGroupToEdit={articleGroupToEdit}
          setArticleGroupToEdit={setArticleGroupToEditFromTable}
          setArticleToEdit={setArticleToEdit}
        />
        <Modal
          title="Confirm Changes to Articles"
          visible={showConfirmChangesModal}
          footer={[
            <Button
              key="article-no-button"
              onClick={() => {
                setMode("");
                toggleConfirmChangesModal(false);
                setArticleToEdit({ content: JSON.stringify(initialContent) });
                setArticleGroupToEdit([]);
                articlesMessage = "";
              }}
            >
              No
            </Button>,
            <Button
              onClick={() => {
                toggleConfirmChangesModal(false);
                toggleAddArticleDrawer(false);
                switch (mode) {
                  case "CREATE":
                    createArticle(articleToEdit);
                    setArticleToEdit({});
                    setArticleGroupToEdit([]);
                    break;
                  case "UPDATE":
                    articleToEdit.dateModied = Date.now().toString();
                    updateArticle(articleToEdit);
                    setArticleToEdit({});
                    setArticleGroupToEdit([]);
                    break;
                  case "DELETE":
                    deleteArticles();
                    break;
                  case "PUBLISH":
                    publishArticles();
                    break;
                  default:
                    break;
                }
                setMode("");
                setArticleToEdit({ content: JSON.stringify(initialContent) });
                setArticleGroupToEdit([]);
              }}
              key="article-yes-button"
              type={"primary"}
            >
              Yes
            </Button>
          ]}
        >
          <Typography.Text>{returnConfirmationMessage(mode)}</Typography.Text>
        </Modal>
      </Layout.Content>
    </Layout>
  );
};

const mapStateToProps = state => {
  const { articleManagement } = state;

  return {
    ...articleManagement
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getArticles: (paginationToken = "") => {
      dispatch(actions.articleManagement.getArticles(paginationToken));
    },
    createArticle: (inputArticle, mode) => {
      dispatch(actions.articleManagement.createArticle(inputArticle));
    },
    updateArticle: (inputArticle, mode) => {
      dispatch(actions.articleManagement.updateArticle(inputArticle, mode));
    },
    deleteArticle: inputArticleId => {
      dispatch(actions.articleManagement.deleteArticle(inputArticleId));
    },
    finishOperation: () => {
      dispatch(actions.articleManagement.finishOperation());
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ArticleManagement);
