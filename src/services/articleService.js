export default ({ headers, config }) => ({
  createArticle: async inputArticle => {
    const { services } = config;
    const response = await fetch(services.createArticleUrl, {
      method: "POST",
      headers,
      body: JSON.stringify({
        ...inputArticle
      })
    });

    return response.json();
  },

  getArticles: async (paginationToken = "") => {
    const { services } = config;

    let getArticlesUrl = services.getArticlesUrl;

    if (paginationToken) {
      getArticlesUrl = getArticlesUrl.concat(
        `?paginationToken=${paginationToken}`
      );
    }

    const response = await fetch(getArticlesUrl, {
      method: "GET",
      headers
    });

    return response.json();
  },

  updateArticle: async inputArticle => {
    const { services } = config;
    const response = await fetch(services.updateArticleUrl(inputArticle.id), {
      method: "POST",
      headers,
      body: JSON.stringify({
        ...inputArticle
      })
    });

    return response.json();
  },

  deleteArticle: async inputArticleId => {
    const { services } = config;

    const response = await fetch(services.deleteArticleUrl(inputArticleId), {
      method: "DELETE",
      headers
    });

    return response.json();
  }
});
