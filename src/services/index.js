import articleService from "./articleService";

export default class API {
  static initiate = host => {
    if (!host) {
      throw new Error({
        message: "config in API class is not defined."
      });
    }

    const config = {
      services: {
        getArticlesUrl: `${host}:4567/api/articles`,
        createArticleUrl: `${host}:4567/api/articles`,
        updateArticleUrl: id => `${host}:4567/api/articles/${id}`,
        deleteArticleUrl: id => `${host}:4567/api/articles/${id}`
      }
    };

    // TODO: These APIs have to be separated into public and private
    const services = headers => ({
      articleManagement: {
        ...articleService({ headers, config })
      }
    });

    const commonHeaders = {
      "Content-Type": "application/json",
      Accept: "application/json"
    };

    return {
      public: () => {
        return services(commonHeaders);
      },
      private: ({ accessToken = "" }) => {
        return services({
          ...commonHeaders
          // to do: access tokens for endpoints
          /* Authorization: accessToken */
        });
      }
    };
  };
}
